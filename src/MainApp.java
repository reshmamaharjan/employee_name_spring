import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.*;
public class MainApp {
    public static void main(String[] args) {
        Scanner inp=new Scanner(System.in);
        System.out.println("Enter how many employees?");
        int num=inp.nextInt();
        List<String> st=new ArrayList<>();
        for(int i=1;i<=num;i++){
            System.out.println("Enter name of employee"+i+":");
           st.add(inp.next());
        }
        ApplicationContext ac=new ClassPathXmlApplicationContext("applicationContext.xml");
        Employee employee=(Employee)ac.getBean("employee");
        employee.setEmployeeInfo(st);
        employee.showEmployeeInfo();
    }
}

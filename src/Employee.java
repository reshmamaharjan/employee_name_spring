import java.util.List;

public class Employee {
    List<String> emp;
    public void setEmployeeInfo(List<String> emp){
        this.emp=emp;
    }
    public void showEmployeeInfo(){
        System.out.println("\nEmployees Details:");
        emp.stream().sorted().forEach(System.out::println);
    }
}
